import json
import requests
import logging


class Aria2c:
    '''
    Example : 

      client = Aria2c('localhost', '6800')

      # print server version
      print(client.getVer())

      # add a task to server 
      client.addUri('http://example.com/file.iso')

      # provide addtional options 
      option = {"out": "new_file_name.iso"}
      client.addUri('http://example.com/file.iso', option)
    '''
    IDPREFIX = "aria2tgbot"
    GET_VER = 'aria2.getVersion'

    def __init__(self, host, token=None):
        self.host = host
        self.token = token
        self.serverUrl = f"http://{host}/jsonrpc"
        self.logger = logging.getLogger("aria2c-rpc")

    def _genPayload(self, method, uris=None, options=None, cid=None):
        cid = IDPREFIX + cid if cid else Aria2c.IDPREFIX
        p = {
            'jsonrpc': '2.0',
            'id': cid,
            'method': method,
            'params': ["token:" + self.token]
        }
        if uris:
            p['params'].append(uris)
        if options:
            p['params'].append(options)
        self.logger.debug(p)
        return p

    def post(self, action, params):
        payloads = self._genPayload(action, *params)
        resp = requests.post(self.serverUrl, data=json.dumps(payloads))
        return resp.json()

    def getVer(self):
        return self.post(Aria2c.GET_VER, [])
