# Telegram bot token
TG_TOKEN = ""
# Aria2 RPC URL
RPC_URL = ""
# Aria2 RPC secret
RPC_SECRET = ""
# Bot admin ID. Bot will accept messages only from that user and will send notifications to this user
ADMIN = ""
# Proxy for telegram
TG_PROXY_URL = None
