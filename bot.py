import logging
import time
import lib
import json
import settings
import telegram
import threading
from hurry.filesize import size
from websocket import create_connection
import html
logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger("aria2-tbot")
ws = create_connection("ws://%s/jsonrpc" % settings.RPC_URL)
aria2 = lib.Aria2c(host=settings.RPC_URL, token=settings.RPC_SECRET)
LOGGER.info("Connected to RPC!")
bot = telegram.Bot(settings.TG_TOKEN, request=telegram.utils.request.Request(
    proxy_url=settings.TG_PROXY_URL))


def _create_file_info(file):
    message = ''
    message += f'- <code>{html.escape(file["uris"][0]["uri"])}</code> '
    message += f'to <code>{html.escape(file["path"])}</code> '
    message += f'({size(int(file["completedLength"]))}/{size(int(file["length"]))})\n'
    return message


def _create_dw_desc(dw):
    methods = {
        "active": "🏃‍♂️ Downloading",
        "paused": "⏸️ Paused",
        "removed": "🗑️ Removed",
        "complete": "✅ Complete",
        "error": "🛑 Error"
    }
    return f'{methods[dw["status"]]} /tellstat_{dw["gid"]}\n'


def watch_for_notifications():
    LOGGER.info("Start watching for notifications")
    while 1:
        notification = json.loads(ws.recv())
        file_info = aria2.post("aria2.tellStatus", [
                               notification["params"][0]["gid"]])["result"]
        if "errorMessage" not in file_info:
            file_info["errorMessage"] = ""
        message = ""
        methods = {
            "aria2.onDownloadStart": {"emoji": "🏁", "string": "started"},
            "aria2.onDownloadPause": {"emoji": "⏸️", "string": "paused"},
            "aria2.onDownloadStop": {"emoji": "⏹", "string": "stopped"},
            "aria2.onDownloadComplete": {"emoji": "✅", "string": "complete"},
            "aria2.onDownloadError": {"emoji": "🛑", "string": "errored cause of " + file_info["errorMessage"]},
            "aria2.onBtDownloadComplete": {"emoji": "✅", "string": "torrent downloaded"}
        }
        message += methods[notification["method"]]["emoji"] + \
            "Download " + methods[notification["method"]]["string"] + "\n"
        for file in file_info["files"]:
            message += _create_file_info(file)
        try:
            bot.sendMessage(settings.ADMIN, message, parse_mode="HTML")
        except telegram.error.NetworkError:
            pass


def add_url(bot, update):
    url = " ".join(update.message.text.split(" ")[1:])
    aria2.post("aria2.addUri", [[url]])


def downloads(bot, update):
    message = []
    downloads = aria2.post("aria2.tellActive", [])["result"]
    downloads += aria2.post("aria2.tellWaiting", [1, 21])["result"]
    downloads += aria2.post("aria2.tellStopped", [1, 21])["result"]
    for download in downloads:
        message.append(_create_dw_desc(download))
    message.reverse()
    update.message.reply_text("\n".join(message))


def aria_ver(bot, update):
    ver = aria2.getVer()["result"]
    message = f"Aria2 version: {ver['version']}\nEnable features:\n"
    message += "\n".join(ver["enabledFeatures"])
    update.message.reply_text(message)


def tellstat(bot, update):
    methods = {
        "active": "🏃‍♂️ Downloading",
        "paused": "⏸️ Paused",
        "removed": "🗑️ Removed",
        "complete": "✅ Complete",
        "error": "🛑 Error"
    }
    message = ''
    gid = update.message.text.split(" ")[0].split("_")[1]
    file_info = aria2.post("aria2.tellStatus", [gid])["result"]
    message += "<b>" + methods[file_info["status"]] + "</b>\n"
    for file in file_info["files"]:
        message += _create_file_info(file)
    if file_info["status"] == "active":
        message += "<b>Pause:</b> /pause_" + gid
    update.message.reply_text(message, parse_mode='HTML')


def pause_dwn(bot, update):
    url = " ".join(update.message.text.split(" ")[0].split("_")[1])
    aria2.post("aria2.pause", [[url]])


threading.Thread(target=watch_for_notifications, daemon=True).start()
COMMANDS = {
    "/addurl": add_url,
    "/aria2ver": aria_ver,
    "/downloads": downloads,
    "/tellstat": tellstat,
    "/pause": pause_dwn
}


def poll():
    update_offset = None
    LOGGER.info("Start watching for telegram updates")
    while 1:
        try:
            updates = bot.getUpdates(update_offset)
            for update in updates:
                if update.message and update.message.text.startswith("/"):
                    if str(update.message.from_user.id) == settings.ADMIN:
                        command = update.message.text.split(
                            " ")[0].split("@")[0].split("_")[0].lower()
                        if command in COMMANDS:
                            COMMANDS[command](bot, update)
                        else:
                            update.message.reply_text(
                                "I don't know %s command" % command)
                update_offset = update.update_id + 1
        except telegram.error.NetworkError as e:
            LOGGER.error(e)
            time.sleep(2)
        except KeyboardInterrupt:
            raise SystemExit


poll()
